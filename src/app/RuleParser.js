const ConditionFunction = require("./model/ConditionFunction");
const ConditionVariable = require("./model/ConditionVariable");
const ConditionConstant = require("./model/ConditionConstant");
const ConditionData = require("./model/ConditionData");
const ConditionWallet = require("./model/ConditionWallet");
const SellMarketAction = require("./model/SellMarketAction");
const BuyMarketAction = require("./model/BuyMarketAction");
const SetVariableAction = require("./model/SetVariableAction");
const Rule = require("./model/Rule");
const HTTPError = require("./error/HTTPError");
const utils = require("./utils");
const functions = require("./functions");
const messages = require("./../../messages.json");

class RuleParser {
  constructor() {
    this.conditionTypes = {
      CALL: { requiredAttributes: ["name", "arguments"], parser: this.parseConditionFunction.bind(this) },
      VARIABLE: { requiredAttributes: ["name"], parser: this.parseConditionVariable.bind(this) },
      CONSTANT: { requiredAttributes: ["value"], parser: this.parseConditionConstant.bind(this) },
      DATA: { requiredAttributes: ["symbol", "from", "until"], parser: this.parseConditionData.bind(this) },
      WALLET: { requiredAttributes: ["symbol"], parser: this.parseConditionWallet.bind(this) }
    };
    this.actionTypes = {
      BUY_MARKET: { requiredAttributes: ["symbol", "amount"], parser: this.parseBuyMarketAction.bind(this) },
      SELL_MARKET: { requiredAttributes: ["symbol", "amount"], parser: this.parseSellMarketAction.bind(this) },
      SET_VARIABLE: { requiredAttributes: ["name", "value"], parser: this.parseSetVariableAction.bind(this) },
    };
  }

  parseRules(rules, user, requiredVariables) {
    let newRules = {};
    if (requiredVariables === undefined)
      requiredVariables = [];

    for (let i = 0; i < rules.length; i++ ) {
      let rule = rules[i];
      let condition = this.parseCondition( rule["condition"], requiredVariables );
      let actions = this.parseActions(rule["action"], requiredVariables);
      newRules[ rule["name"] ] = new Rule(rule["name"], condition, actions, user, rule, requiredVariables);
    }

    return newRules;
  }

  parseArguments(conditionArgs,requiredVariables) {
    if ( Array.isArray(conditionArgs) )
      return this.parseConditions(conditionArgs,requiredVariables);

    const parsedCondition = this.parseCondition(conditionArgs,requiredVariables);
    return [ parsedCondition ];
  }

  parseCondition(conditionJSON, requiredVariables) {
    // console.log("Parsing condition:", conditionJSON);
    if (!Object.keys(conditionJSON).includes("type"))
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        messages["RULE_CONDITION_MUST_CONTAIN_TYPE"]);
    return this.parseConditions( [conditionJSON], requiredVariables )[0];
  }

  parseConditions(conditionsJSON, requiredVariables) {
    let conditions = [];
    conditionsJSON.forEach(condition => {
      // console.log("Parsing condition:", condition);
      this.checkAttributes(condition, this.conditionTypes);
      conditions.push(this.conditionTypes[condition["type"]].parser(condition, requiredVariables));
    });
    return conditions;
  }

  parseActions(actionsJSON, requiredVariables) {
    let actions = [];
    if (!Array.isArray(actionsJSON))
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        messages["RULE_ACTION_MUST_BE_ARR"]);
    actionsJSON.forEach(action => {
      // console.log("Parsing action:", action);
      this.checkAttributes(action, this.actionTypes);
      actions.push(this.actionTypes[action["type"]].parser(action, requiredVariables));
    });
    return actions;
  }

  checkAttributes(condition, conditionTypes) {
    if ( !Object.keys(conditionTypes).includes(condition["type"]) )
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["RULE_ATTRIBUTE_MISSING"] + " type");
    conditionTypes[condition["type"]].requiredAttributes.forEach(requiredAttribute => {
      if (condition[requiredAttribute] === undefined)
        throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
          messages["RULE_ATTRIBUTE_MISSING"] + " " + requiredAttribute + " " + messages["RULE_ATTRIBUTE_MISSING_2"] + " " + condition["type"]);
    });
  }

  parseConditionFunction(condition, requiredVariables) {
    return new ConditionFunction(condition["name"], functions[condition["name"]], this.parseArguments(condition["arguments"], requiredVariables));
  }

  parseConditionVariable(condition, requiredVariables) {
    if (! requiredVariables.includes(condition["name"])) {
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["VARIABLE_REQUIRED_NOT_SPECIFIED"]);
    }

    return new ConditionVariable(condition["name"]);
  }

  parseConditionConstant(condition, requiredVariables) {
    return new ConditionConstant(condition["value"]);
  }

  parseConditionData(condition, requiredVariables) {
    const defaultValue = ( condition["default"] !== undefined ) ? this.parseArguments(condition["default"], requiredVariables) : undefined;
    return new ConditionData(
      utils.createSymbol(condition["symbol"]), condition["from"], condition["until"], defaultValue
    );
  }

  parseConditionWallet(condition, requiredVariables) {
    return new ConditionWallet(condition["symbol"]);
  }

  parseBuyMarketAction(action, requiredVariables) {
    return new BuyMarketAction(utils.createSymbol(action["symbol"]), this.parseCondition(action["amount"], requiredVariables));
  }

  parseSellMarketAction(action, requiredVariables) {
    return new SellMarketAction(utils.createSymbol(action["symbol"]), this.parseCondition(action["amount"], requiredVariables));
  }

  parseSetVariableAction(action, requiredVariables) {
    return new SetVariableAction(action["name"], this.parseCondition(action["value"], requiredVariables));
  }

}

module.exports = RuleParser;
