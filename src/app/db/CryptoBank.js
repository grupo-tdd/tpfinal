const database = require('./db');
const Sequelize = require('sequelize');
const dotenv = require('dotenv').config();

const CryptoBank = database.define('CryptoBank', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement:true },

  symbol: { type: Sequelize.STRING(process.env.SYMBOL_MAX_LEN),
    allowNull: false,
    validate: { notEmpty: true } },

  timestamp: { type: Sequelize.BIGINT,
    allowNull: false,
    validate: { notEmpty: true } },

  busdValue: { type: Sequelize.DOUBLE(10, 3),
    allowNull: false,
    validate: { notEmpty: true } },
});

module.exports = CryptoBank;
