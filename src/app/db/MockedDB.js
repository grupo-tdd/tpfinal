const sequilizeMock = require("sequelize-mock");
const moment = require('moment-timezone');


var DBConnectionMock = new sequilizeMock();
var databaseMock = DBConnectionMock.define("aDatabase",{ autoQueryFallback: true });
databaseMock.$queueResult([
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 50,
            busdValue: 500.0
        }),
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 100,
            busdValue: 480.0
        })]
);

databaseMock.$queueResult([
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 50,
            busdValue: 500.0
        }),
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 100,
            busdValue: 480.0
        })]
);

databaseMock.$queueResult([
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 50,
            busdValue: 500.0
        }),
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 100,
            busdValue: 480.0
        })]
);

databaseMock.$queueResult([
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 50,
            busdValue: 500.0
        }),
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 100,
            busdValue: 480.0
        })]
);

databaseMock.$queueResult([
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 50,
            busdValue: 500.0
        }),
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 100,
            busdValue: 480.0
        })]
);


databaseMock.$queueResult([
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 50,
            busdValue: 500.0
        }),
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 100,
            busdValue: 480.0
        })]
);


databaseMock.$queueResult([
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 50,
            busdValue: 500.0
        }),
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 100,
            busdValue: 480.0
        })]
);


databaseMock.$queueResult([
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 50,
            busdValue: 500.0
        }),
    databaseMock.build({
            symbol:"TDDUSDT",
            timestamp: moment().tz("America/Buenos_Aires").valueOf() - 100,
            busdValue: 480.0
        })]
);


module.exports = databaseMock

