const dotenv = require('dotenv').config();

function HTTPError(status = process.env.HTTP_500_INTERNAL_SERVER_ERROR, message, fileName, lineNumber) {
  var instance = new Error(message, fileName, lineNumber);
  instance.name = 'HTTPError';
  instance.status = status;
  Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
  if (Error.captureStackTrace) {
    Error.captureStackTrace(instance, HTTPError);
  }
  return instance;
}

HTTPError.prototype = Object.create(Error.prototype, {
  constructor: {
    value: Error,
    enumerable: false,
    writable: true,
    configurable: true
  }
});

if (Object.setPrototypeOf){
  Object.setPrototypeOf(HTTPError, Error);
} else {
  HTTPError.__proto__ = Error;
}

module.exports = HTTPError;
