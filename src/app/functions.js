function compareArrayElements(args, comparison) {
  return args.slice(1)
             .reduce( (accum, curr) => { 
                return [curr, accum[1] && comparison(accum[0], curr)] 
              }, [args[0], true] )[1];
}

function arrayAvg(arr) {
  return arr.reduce((accum, curr) => { return accum + curr }, 0) / arr.length;
}

function arrayStd(arr) {
  if ( arr.length === 0 ){
    throw new Error("STDDEV de un conjunto vacío");
  }
  let mean = arrayAvg(arr);
  arr = arr.map((k)=>{ return (k - mean) ** 2 })
  let sum = arr.reduce((accum, curr)=> accum + curr, 0);
  return Math.sqrt(sum / arr.length)
}

function checkIfArrHasOnlyType(args, type, operation){

  let types = args.map(arg => { return (typeof arg == type) });
  types.push(true);
  if (! functions["=="](types)){
    throw new Error("La operación "+operation+" solo acepta valores de tipo "+type);
  }
}

function checkIfDividedByZero(args){
    if ( args[1] === 0 ){
      throw new Error("No se puede dividir por 0");
    }
}

function checkIfCantArgsIsN(args, operation, n){
  if ( args.length !== n ){
    throw new Error("La operación "+operation+" solo acepta "+n+" argumento/s");
  }
}

// args is always an array
const functions = {
  "==": (args) => { return compareArrayElements(args, (arg1, arg2) => { return arg1 == arg2 }) },
  "DISTINCT": (args) => { return compareArrayElements(args, (arg1, arg2) => { return arg1 != arg2 }) },
  "<": (args) => { return compareArrayElements(args, (arg1, arg2) => { return arg1 < arg2 }) }, 
  "<=": (args) => { return compareArrayElements(args, (arg1, arg2) => { return arg1 <= arg2 }) }, 
  ">": (args) => { return compareArrayElements(args, (arg1, arg2) => { return arg1 > arg2 }) }, 
  ">=": (args) => { return compareArrayElements(args, (arg1, arg2) => { return arg1 >= arg2 }) }, 
  "NEGATE": (args) => { checkIfArrHasOnlyType(args,"number","'NEGATE'"); checkIfCantArgsIsN(args,"'NEGATE'",1);return ~args[0] },
  "-": (args) => { checkIfArrHasOnlyType(args,"number","'-'"); checkIfCantArgsIsN(args,"'-'",2); return args[0] - args[1] },
  "/": (args) => { checkIfArrHasOnlyType(args,"number","'/'"); checkIfDividedByZero(args); checkIfCantArgsIsN(args,"'/'",2); return args[0] / args[1] },
  "+": (args) => { checkIfArrHasOnlyType(args,"number","'+'"); return args.slice(1).reduce((accum, curr) => { return accum + curr }, args[0]) },
  "*": (args) => { checkIfArrHasOnlyType(args,"number","'*'"); return args.slice(1).reduce((accum, curr) => { return accum * curr }, args[0]) },
  "MIN": (args) => { checkIfArrHasOnlyType(args,"number","'MIN'"); return args.slice(1).reduce((accum, curr) =>  { return Math.min(accum, curr) }, args[0]) },
  "MAX": (args) => { checkIfArrHasOnlyType(args,"number","'MAX'"); return args.slice(1).reduce((accum, curr) =>  { return Math.max(accum, curr) }, args[0]) },
  "AVERAGE": (args) => { checkIfArrHasOnlyType(args,"number","'AVERAGE'"); return arrayAvg(args) },
  "STDDEV": (args) => { checkIfArrHasOnlyType(args,"number","'STDDEV'"); return arrayStd(args) },
  "NOT": (args) => { checkIfArrHasOnlyType(args,"boolean","'NOT'"); checkIfCantArgsIsN(args,"'NOT'",1); return !args[0] },
  "AND": (args) => { checkIfArrHasOnlyType(args,"boolean","'AND'"); return args.reduce((accum, curr) => { return accum && curr }, true) },
  "OR": (args) => { checkIfArrHasOnlyType(args,"boolean","'OR'"); return args.reduce((accum, curr) => { return accum || curr }, false) },
  "FIRST": (args) => { return args[0] },
  "LAST": (args) => { return args[args.length - 1] },
}

module.exports = functions