const cors = require('cors');
const dotenv = require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const database = require('./db/db');
const CryptoBank = require('./db/CryptoBank');
const WebSocket = require('ws');
const moment = require('moment-timezone');
const date = require('./date');
const fs = require('fs');

const EventCompatible = require("./EventCompatible");
const CryptoManager = require("./web/CryptoManager");
const RuleManager = require("./web/RuleManager");
const UserManager = require("./web/UserManager");
const MailManager = require("./web/MailManager");

const utils = require('./utils');

class App extends EventCompatible {
  constructor() {
    super();
    this.app = express();
    this.app.use( cors() );
    this.app.use(bodyParser.json());

    this.cryptoMgr = new CryptoManager();
    this.ruleMgr = new RuleManager();
    this.userMgr = new UserManager(this.uptime);

    // Solo para la presentación
    fs.appendFileSync('cotizaciones.txt', "============================\n", err => {
      if (err) {
        console.error(err);
      }
    })
  }

  async syncDB() {
    // "sync()" creates the database table for our model(s),
    // if we make .sync({force: true}), the db is dropped it first if it already existed
    await database.sync({force: true});

    this.app.listen(process.env.NODE_DOCKER_PORT, () => {
      console.log(`Listening on port ${process.env.NODE_DOCKER_PORT}`)
    });
  }

  defineEvents() {
    this.cryptoMgr.defineEvents(this.app);
    this.userMgr.defineEvents(this.app);
    this.app.post( process.env.USER_URI + process.env.RULE_URI, 
      this.createRule.bind(this) );
    this.app.put( process.env.USER_URI + process.env.RULE_URI, 
      this.updateRule.bind(this) );
    this.app.get( process.env.USER_URI + process.env.RULE_URI, 
        this.getRules.bind(this) );
    this.app.get( process.env.USER_URI + process.env.WALLET_URL,
      this.getAccountInfo.bind(this) );
    
    this.app.post( process.env.LOG_IN_GOOGLE, this.logInGoogle.bind(this) );
    this.app.post( process.env.USER_URI + process.env.SHARE_RULE, this.sendShareRuleRequest.bind(this) );
    this.app.get( process.env.USER_URI + process.env.SHARE_RULE, this.acceptShareRuleRequest.bind(this) );
    this.app.delete(process.env.USER_URI + process.env.RULE_URI, this.deleteRule.bind(this));
    this.app.get(process.env.USER_URI + process.env.ALL_ORDERS_URL, this.getUserAllOrders.bind(this));
    this.app.get( process.env.USER_URI + process.env.RULE_URI + process.env.FILE_URI, 
      this.getRuleFile.bind(this) );
  }

  async logInGoogle(req, res) {
    await this.handleEvent(req, res, `POST ${process.env.USER_URI}`,
      async (req, res) => {
        const { name, surname, email, uid } = req.body

        try {
          this.userMgr.checkIfUserDoesNotExist(email);
          console.log("Existing user with google account");

        } catch (err) {
          console.log(`New user with google ${email} account`);
          this.userMgr.createAUser(email, uid, name, surname);
        }
        finally{
          req.body = {
            email: email,
            password: uid
          }
          this.userMgr.getUserHash(req, res);
        }
      });
  }

  async createRule(req, res) {

    await this.handleEvent(req, res, `POST ${process.env.USER_URI + process.env.RULE_URI}`,
      async (req, res) => {
        const userId = req.headers["x-auth-token"];
        const email = this.userMgr.getEmail(userId);
        const rules = (req.body.rules === undefined ? [] : req.body.rules);
        const requiredVariables = (req.body.requiredVariables === undefined ? [] : req.body.requiredVariables);
        await this.ruleMgr.createRules(rules, this.userMgr.getUser(email), requiredVariables);

        // utils.respond(res, process.env.HTTP_200_OK, {rules:req.body.rules} );
        utils.respond(res, process.env.HTTP_200_OK, {body: "Regla creada."} );
      } );
  }

  async updateRule(req, res) {
    await this.handleEvent(req, res, `PUT ${process.env.USER_URI + process.env.RULE_URI}`,
      async (req, res) => {
        const userId = req.headers["x-auth-token"];
        const email = this.userMgr.getEmail(userId);
        const rules = (req.body.rules === undefined ? [] : req.body.rules);
        const requiredVariables = (req.body.requiredVariables === undefined ? [] : req.body.requiredVariables);
        await this.ruleMgr.updateRule(req.body.name, rules, this.userMgr.getUser(email), requiredVariables);
        // utils.respond(res, process.env.HTTP_200_OK, {rules:req.body.rules} );
        utils.respond(res, process.env.HTTP_200_OK, {body: "Regla actualizada."} );
      } );
  }

  async getRules(req, res) {
    await this.handleEvent(req, res, `GET ${process.env.USER_URI + process.env.RULE_URI}`,
      async (req, res) => {
        const userId = req.headers["x-auth-token"];
        let email = this.userMgr.getEmail(userId);
        let rulesInfo = await this.ruleMgr.getRulesInfo(email);
        utils.respond(res, process.env.HTTP_200_OK, rulesInfo );
      } );
  }

  async sendShareRuleRequest(req, res) {
    await this.handleEvent(req, res, `POST ${process.env.SHARE_RULE}`,
      async (req, res) => {
        const srcId = req.headers["x-auth-token"];
        const srcEmail = this.userMgr.getEmail(srcId);
        const dstEmail = req.body.guestEmail;
        const ruleName = req.body.rule;
        const permissionType = req.body.permission;
        const dstUser = this.userMgr.getUser(dstEmail);
        const mailer = new MailManager();
        this.userMgr.checkIfUserDoesNotExist(dstEmail);
        this.ruleMgr.checkIfRuleDoestNotExist(ruleName, srcEmail);
        this.ruleMgr.checkIfRuleExist(ruleName, dstEmail);
        mailer.sendInvitationEmail(srcEmail, dstEmail, ruleName , permissionType);
        await this.ruleMgr.createPendingRule(srcEmail, dstUser, ruleName, permissionType);
        utils.respond(res, process.env.HTTP_200_OK, {message: `Se envio el mail a ${dstEmail} para compartir la regla ${ruleName}`} );
      } );
  }


  async acceptShareRuleRequest(req, res) {
    await this.handleEvent(req, res, `GET ${process.env.SHARE_RULE}`,
      async (req, res) => {
        const useremail = req.query.user;
        const ruleName = req.query.rule;
        this.userMgr.checkIfUserDoesNotExist(useremail);
        this.ruleMgr.checkIfPendingRuleDoestNotExist(ruleName, useremail);
        await this.ruleMgr.shareRule(ruleName, useremail);
        utils.respond(res, process.env.HTTP_200_OK, {message: `La regla ${ruleName} fue compartida con el usuario ${useremail} exitosamente`} );
      } );
  }

  async getAccountInfo(req, res) {
    await this.handleEvent(req, res, `GET ${process.env.USER_URI + process.env.WALLET_URL}`,
      async (req, res) => {
        const id = req.headers["x-auth-token"];
        let symbol = req.query.symbol;
        if (symbol)
          symbol = symbol.toUpperCase();
        await this.userMgr.getAccountInfo(id, res, symbol);
      });
  }

  async deleteRule(req, res){
    await this.handleEvent(req, res, `DELETE ${process.env.USER_URI + process.env.RULE_URI}`,
      async (req, res) => {
        const userId = req.headers["x-auth-token"];
        const useremail = this.userMgr.getEmail(userId);
        const ruleName = req.body.rule;
        await this.ruleMgr.deleteRule(ruleName,useremail);
        utils.respond(res, process.env.HTTP_200_OK, {message: `La regla ${ruleName} ha sido borrada exitosamente`} );
      });
  }

  async getUserAllOrders(req, res){
    await this.handleEvent(req, res, `GET ${process.env.USER_URI + process.env.ALL_ORDERS_URL}`,
      async (req, res) => {
        const userId = req.headers["x-auth-token"];
        const symbol = req.query.symbol;
        const useremail = this.userMgr.getEmail(userId);
        const allOrders = await this.userMgr.getUserAllOrders(useremail, symbol);
        utils.respond(res, process.env.HTTP_200_OK, {allOrders: allOrders} );
      });
  }

  async getRuleFile(req, res){
    await this.handleEvent(req, res, `GET ${process.env.USER_URI + process.env.RULE_URI}`,
      async (req, res) => {
        const userId = req.headers["x-auth-token"];
        const useremail = this.userMgr.getEmail(userId);
        const ruleName = req.query.ruleName;
        const rule = await this.ruleMgr.getRuleFile(ruleName,useremail);
        utils.respond(res, process.env.HTTP_200_OK, {file: rule} );
      });
  }

  async updateCryptoValues(event) {
    try {
      const price = Number(JSON.parse(event.data)["b"][0][0]);
      const symbol = JSON.parse(event.data).s;
      // It can be turned back into moment with the string and the format
      // in order to make date calculations.
      const now = moment().tz(process.env.TIMEZONE).valueOf();

      let percentage = 0;
      let min_percentage = Number(process.env.CHANGE_TO_UPDATE_RULES);
      let latestPrice = 0;
      let latestId = await CryptoBank.max('id', {
        where: {symbol: symbol}
      } );

      if (latestId) {
        latestPrice = await CryptoBank.findOne({
          where: {symbol, id: latestId}
        } ).then(result => {
          return result.busdValue;
        } );

        percentage = Number(process.env.MINIMAL_PRICE_CHANGE);
      }

      if ( ( latestPrice * (1 - percentage) > price ) ||
        ( latestPrice * (1 + percentage) < price ) ) {
        const log_msg = "- " + utils.getPrintableTimeStamp() + " " + symbol + " " + price;
        console.log(log_msg);

        // Solo para la presentación
        if (symbol === "BNBUSDT") {
          fs.appendFileSync('cotizaciones.txt', log_msg + "\n", err => {
            if (err) {
              console.error(err);
            }
          })
        }

        await CryptoBank.create({
          symbol: symbol,
          timestamp: now,
          busdValue: price
        });
      }

      if ( ( latestPrice * (1 - min_percentage) > price ) ||
        ( latestPrice * (1 + min_percentage) < price ) ) {
        await this.ruleMgr.executeRules();
      }

    } catch(e){
      console.log("Binance did not respond correctly.");
    }
  }

  addWebSocket(url) {
    new WebSocket(url).addEventListener( 'message', this.updateCryptoValues.bind(this) );
  }
}

const main = new App();

main.syncDB().then( () => {
  main.defineEvents();
} );

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.BNBUSDT.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.USDTBNB.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.BTCUSDT.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.ETHUSDT.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.USDTBTC.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.USDTETH.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.BTCBUSD.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.ETHBUSD.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);

main.addWebSocket(process.env.BINANCE_STREAM_URL +
  process.env.BNBBUSD.toLowerCase() +
  process.env.BINANCE_STREAM_UPDATE_FREQUENCY);
