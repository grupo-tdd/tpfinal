const CryptoTransactionAction = require("./CryptoTransactionAction");
const utils = require("../utils");

class BuyMarketAction extends CryptoTransactionAction {
    constructor(symbol, condition){
        super(symbol, condition);
    }

    async executeAction(user){
        const amount = await this.checkIfAmountIsNumeric(user);
        console.log("\n");
        console.log("BUY MARKET " + utils.getPrintableTimeStamp());
        console.log("BUYING: ", this.symbol);
        console.log("amount:", amount);
        const endpoint = process.env.TESTBINANCE_ORDER_URL +
          utils.createBuyMarketQuery(this.symbol, amount, user.getApiSecret());
        return await this.sendPostRequest(endpoint, user.getApiKey());
    }
}

module.exports = BuyMarketAction;