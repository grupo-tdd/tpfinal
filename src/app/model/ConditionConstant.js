const Condition = require("./Condition");
const HTTPError = require("../error/HTTPError");
const messages = require("./../../../messages.json");

class ConditionConstant extends Condition {
  constructor(value) {
    super();
    this.value = value;    
    if (value === undefined)
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["CONSTANT_UNDEFINED"])
  }

  check(user) {
    // TODO: Refactor needed, the constant does not use the user
    return this.value;
  }

  toString() {
    return "Condicion CONSTANT: " + this.value.toString();
  }
}

module.exports = ConditionConstant;