const CryptoBank = require('../db/CryptoBank');
const moment = require('moment-timezone');
const { Op } = require("sequelize");
const Condition = require('./Condition');
const utils = require("../utils");
const HTTPError = require("../error/HTTPError");
const messages = require("../../../messages.json");
const fs = require('fs');

class ConditionData extends Condition {
  constructor(symbol, from, until, defaultCondition) {
    super();
    this.checkParams(symbol, from, until);
    this.symbol = symbol;
    this.from = from;
    this.until = until;
    this.defaultCondition = defaultCondition;
    this.values = [];
  }

  checkParams(symbol, from, until) {
    if (!utils.symbolPairIsValid(symbol))
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["INVALID_SYMBOL"] + " " + symbol);
    if (typeof from !== "number" || typeof until !== "number")
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["INVALID_DATA_PARAM_TYPE"]);
    if (from > process.env.MAX_WINDOW_IN_SECONDS || 
        until > process.env.MAX_WINDOW_IN_SECONDS ||
        from < 0 ||
        until < 0 ||
        from < until)
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["INVALID_DATA_WINDOW"]);
  }

  async getValuesOf(aSymbol, from, until) {
    let init = moment().subtract(from, 'seconds').valueOf();
    let end = moment().subtract(until, 'seconds').valueOf();
    let cryptoValues = [];

    await CryptoBank.findAll( {
      where: {
        [Op.and]: [
          { symbol: aSymbol },
          { timestamp: { [Op.gte]: init } },
          { timestamp: { [Op.lte]: end } }
        ]
      }
    } ).then( (response) => {
        response.forEach( (element) => { 
          cryptoValues.push( Number(element.dataValues.busdValue) );
      } );
    } );

    // Solo para la presentación
    fs.appendFileSync('presentation_info.txt', cryptoValues + "\n", err => {
      if (err) {
        console.error(err);
      }
    })

    return cryptoValues;
  }

  async check(user) {
    this.values =  await this.getValuesOf(this.symbol,this.from, this.until);
    if ( this.values.length === 0 ){
      if ( this.defaultCondition === undefined ){
        throw new Error("Error: Condición DATA sin valor default ni historial");
      }
      this.values = await Promise.all(this.defaultCondition.map(async(condition) => {
        return await condition.check(user);
      }));
    }
    return this.values;
  }

  toString() {
    let condString = ("Condicion DATA " + this.symbol.toString() + ", from:" + this.from.toString() +
    ", until:" + this.until.toString() + ", con valores: ");
    return (condString + this.values.toString());
  }
}

module.exports = ConditionData;