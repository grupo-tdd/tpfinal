const Condition = require("./Condition");
const utils = require("../utils");
const HTTPError = require("../error/HTTPError");
const messages = require("../../../messages.json");

class ConditionFunction extends Condition {
  constructor(name, condFunction, args) {
    super();
    if (condFunction === undefined)
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["BAD_FUNCTION_NAME"]);
    this.name = name;
    this.condFunction = condFunction;
    this.args = args;
    this.argsValues = [];
  }

  async check(user) {
    let args = await Promise.all(this.args.map(async function(condArg){ return await condArg.check(user) }));
    // console.log("================");
    // console.log(utils.flatArr(args));
    this.argsValues = utils.flatArr(args);
    this.argsValuesToString = utils.flatArr(await Promise.all(this.args.map(async function(condArg){ return (await condArg.toString()) + "\n" })));
    return this.condFunction(utils.flatArr(args));
  }

  toString() {
    return ("Condicion FUNCTION " + this.name + " con valores: " + this.argsValues.toString()
      + "\n" + this.argsValuesToString.toString()
    );
  }
}

module.exports = ConditionFunction;