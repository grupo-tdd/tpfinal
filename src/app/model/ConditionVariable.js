const Condition = require("./Condition");

class ConditionVariable extends Condition {
  constructor(name) {
    super();
    this.name = name;
    this.value = "undefined";
  }

  getName() {
    return this.name;
  }

  check(user) {
    this.value = user.getVariable(this.name);
    return this.value;
  }

  toString() {
    return "Condicion VARIABLE " + this.name + " con valor: " + this.value.toString();
  }
}

module.exports = ConditionVariable;