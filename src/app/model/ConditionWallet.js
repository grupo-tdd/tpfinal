const utils = require("../utils");
const HTTPError = require("../error/HTTPError");
const messages = require("../../../messages.json");

class ConditionWallet {
  constructor(symbol) {
    this.symbol = symbol;
    if (!utils.symbolIsValid(symbol))
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["INVALID_SYMBOL"] + " " + symbol);

    this.value = "undefined";
  }

  getSymbol() {
    return this.symbol;
  }

  async check(user) {
    this.value = await user.getWalletInfo(this.symbol);
    return this.value;
  }

  toString() {
    return "Condicion WALLET " + this.symbol + " con valor: " + this.value.toString();
  }
}

module.exports = ConditionWallet;