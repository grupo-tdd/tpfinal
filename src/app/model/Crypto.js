const CryptoBank = require('../db/CryptoBank');

class Crypto {
  constructor(name) {
    this.name = name;
    this.busdValue = 0;
  }

  async getBusdValue()  {
    const symbol = this.name;

    let maxId = await CryptoBank.max('id', {
      where: {symbol: symbol}
    } );

    this.busdValue = await CryptoBank.findOne( {
      where: {symbol, id: maxId}
    } ).then(result => {
      return result.busdValue;
    } );

    console.log("===== " + this.busdValue);
    return this.busdValue;
  }

  getStr() {
    return this.name;
  }
}

module.exports = Crypto;
