require ('isomorphic-fetch');
const HTTPError = require("../error/HTTPError");
const utils = require("../utils");
const messages = require("../../../messages.json");

class CryptoTransactionAction {
  constructor(symbol, condition){
    this.symbol = symbol;
    this.amount = condition;
    if (!utils.symbolPairIsValid(symbol)) {
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["INVALID_SYMBOL"] + " " + symbol);
    }
  }
    
  async sendPostRequest(endpoint, apiKey){
    // console.log("URL to fetch:", endpoint);
    const res = await fetch(endpoint, {
      method: 'post',
      headers: this.getHeader(apiKey)
    }).then(res => res.json())
      .then(json => json)
      .catch(err => err);
    //console.log( "Response: ",res);
    //console.log("\n\n")
    return res;
  }

  getHeader(apiKey){
    let header = {};
    header["Content-Type"] = "application/json";
    header["Connection"] = "keep-alive";
    header["Content-Length"] = 0;
    header["Accept-Encoding"] = "gzip, deflate, br";
    header["Accept"] = "*/*";
    header["X-MBX-APIKEY"] = apiKey;
    return header;
  }

  async checkIfAmountIsNumeric(user){
    const amount = await this.amount.check(user);
    if ( typeof amount !== "number" ){
      throw new Error("Error: Tipo de dato invalido, el valor 'amount' de las Condiciones tiene que ser de tipo numerico")
    }
    return amount;
   }

}

module.exports = CryptoTransactionAction;
