const utils = require("../utils");
const messages = require("../../../messages.json");

class Rule {
  constructor(name, condition, actions, user, file, requiredVariables) {
    this.name = name;
    this.condition = condition;
    this.actions = actions;
    this.user = user;
    this.executed = 0;
    this.failRule = false;
    this.editionPermission = true;
    this.requiredVariables = requiredVariables;
    this.requiredVariablesExist = true;
    this.file = {
      requiredVariables : this.requiredVariables,
      rules: [file]
    }
  }

  getName() {
    return this.name;
  }

  checkIfRequiredVariablesExist(){
    if (!this.requiredVariablesExist)
      this.executed = 0;
    this.requiredVariablesExist = true;
    this.requiredVariables.forEach(varName => {
      if ( this.user.getVariable(varName) === undefined ){
        this.requiredVariablesExist = false;
        this.executed = this.name + " no ejecutó porque falta " + varName;
        // console.log(this.executed);
      }
    });
    return this.requiredVariablesExist;
  }

  getRequiredVariables(){
    return this.requiredVariables;
  }

  changeUser(user){
    this.user = user;
    this.executed = 0;
    this.failRule = false;
  }
  setReadOnlyPermission(){
     this.editionPermission = false;
  }
  setEditionPermission(){
    this.editionPermission = true;
  }

  getEditionPermission(){
     return this.editionPermission;
  }

  async checkConditions() {
    return (( ! this.failRule ) && this.checkIfRequiredVariablesExist() && await this.verifyCondition());
  }

  async checkIfConditionIsBoolean(){
    const condition = await this.condition.check(this.user);
    if ( typeof condition !== "boolean" ) {
      throw new Error(messages["RULE_CONDITION_NOT_BOOLEAN"]);
    }
    return condition
  }

  async verifyCondition(){
    try{
      return await this.checkIfConditionIsBoolean();;
    }
    catch( e ){
      this.executed = e.message;
      this.failRule = true;
    }

    return false;
  }

  async executeActions() {
    this.executed += 1;
    for ( let key in this.actions ){
      await this.actions[key].executeAction(this.user)
      .catch(e =>{
        console.log(e.message);
        this.executed = messages["RULE_ERROR_EXECUTING_ACTION"] + e.message;
        this.failRule = true;
      });
      if ( this.failRule ) 
        break;
    }
  }

  timesExecuted() {
    return this.executed;
  }

  async execute() {
    if ( await this.checkConditions() ) {
      console.log("Se cumplio la regla con la condicion:\n" + this.condition.toString() + "\n");
      await this.executeActions();
    }
  }

  setJsonFile(file){
    this.file = file;
  }

  getJsonFile(){
    return this.file;
  }
}

module.exports = Rule;