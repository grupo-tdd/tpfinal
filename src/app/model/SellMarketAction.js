const CryptoTransactionAction = require("./CryptoTransactionAction");
const utils = require("../utils");

class SellMarketAction extends CryptoTransactionAction {
    constructor(symbol, condition){
        super(symbol,condition);
    }

    async executeAction(user){
        const amount = await this.checkIfAmountIsNumeric(user);
        console.log("\n");
        console.log("SELL MARKET" + utils.getPrintableTimeStamp());
        console.log("SELLING: ", this.symbol);
        console.log("amount:", amount);
        const endpoint = process.env.TESTBINANCE_ORDER_URL +
          utils.createSellMarketQuery(this.symbol, amount, user.getApiSecret());
        return await this.sendPostRequest(endpoint, user.getApiKey());
    }

}

module.exports = SellMarketAction;