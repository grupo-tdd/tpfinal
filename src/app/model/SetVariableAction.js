const utils = require("../utils");

class SetVariableAction {
    constructor(symbol, condition){
      this.symbol = symbol;
      this.amount = condition;
    }

    async executeAction(user){
      console.log("SET VARIABLE " + utils.getPrintableTimeStamp());
      let amount = await this.amount.check(user);
      user.setVariable(this.symbol, amount);
      console.log("The variable", this.symbol, "is now equal to", user.getVariable(this.symbol));
    }
}

module.exports = SetVariableAction;