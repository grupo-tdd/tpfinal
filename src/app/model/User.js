const EventCompatible = require("../EventCompatible");
const HTTPError = require("../error/HTTPError");
const utils = require("../utils");
const Rule = require("./Rule");
const BinanceRest = require('binance');

class User {
  constructor(email, password, name, surname, passwordHashed = false) {
    this.setEmail(email);
    this.setPassword(password, passwordHashed);
    this.name = name;
    this.surname = surname;
    this.variables = {};
    this.apiKey = process.env.BINANCE_API_KEY;
    this.apiSecret = process.env.BINANCE_API_SECRET;
    this.binance = this.getBinance();
  }

  checkUserIsValid(email, password) {
    return ( (this.email === email) &&
      ( this.password === utils.getHash(password) )
    );
  }

  updateUser(newAttributes) {
    let previousAttributes = {
      name: this.name,
      surname: this.surname,
      password: this.password,
    }
    try{
      this.setName(newAttributes.name || previousAttributes.name);
      this.setSurname(newAttributes.surname || previousAttributes.surname);

      if (newAttributes.password) {
        this.setPassword(newAttributes.password, false);
      } else {
        this.setPassword(previousAttributes.password, true);
      }
    } catch(error) {
      this.setName(previousAttributes.name);
      this.setSurname(previousAttributes.surname);
      this.setPassword(previousAttributes.password, true);
      throw error;
    }
  }

  getUserId(uptime) {
    return utils.jwtSign(this.email, uptime);
  }

  setName(name) {
    utils.checkStringLength(name,
      process.env.MIN_STR_LEN,
      process.env.MAX_STR_LEN,
      `Longitud invalida del atributo name, deberia tener entre [${process.env.MIN_STR_LEN}, ${process.env.MAX_STR_LEN}]`);
    utils.isAlphabeticString(name, "Nombre inválido");
    this.name = name;
  }

  setSurname(surname) {
    utils.checkStringLength(surname,
      process.env.MIN_STR_LEN,
      process.env.MAX_STR_LEN,
      `Longitud invalida del atributo surname, deberia tener entre [${process.env.MIN_STR_LEN}, ${process.env.MAX_STR_LEN}]`);
    utils.isAlphabeticString(surname, "Apellido inválido");
    this.surname = surname;
  }

  setEmail(email) {
    utils.checkInvalidEmail(email);
    this.email = email;
  }

  setPassword(password, passwordHashed = false) {
    utils.checkStringLength(password,
      process.env.MIN_PASS_LEN,
      process.env.MAX_STR_LEN,
      `Longitud invalida del atributo password, deberia tener entre [${process.env.MIN_PASS_LEN}, ${process.env.MAX_STR_LEN}]`);
    this.password = passwordHashed ? password : utils.getHash(password);
  }

  findSymbol(balances, symbol) {
    let result = balances.filter( (balance) =>
      { return balance.asset === symbol } );

    if (result.length === 0) {
      return undefined;
    }

    return Number(result[0].free);
  }

  async getWalletInfo(symbol) {
    if (!symbol)
      return await this.binance.account().then(response => { return { balances: response["balances"] }; });
    
    return await this.binance.account()
      .then( (response) => {
        return this.findSymbol(response["balances"], symbol);
      } ).catch( (e) => {
        console.log(e);
      } );
  }

  checkIfVariableExists(variableName) {
    if (this.variables[variableName] !== undefined) {
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        process.env.VARIABLE_ALREADY_EXISTS);
    }
  }

  setVariable(name, value) {
    if ( typeof name !== "string" ){
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, 
        "Error: Tipo de dato invalido, el nombre de la variable solo puede ser string");
    }
    if ( typeof value !== "number" &&  typeof value !== "boolean" && typeof value !== "string" && typeof value !== "undefined" ){
       throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, 
        "Error: Tipo de dato invalido, una variable solo puede guardar tipo de dato numerico, booleano o string");
    }
    this.variables[name] = value;
  }

  getVariable(name) {
    return this.variables[name];
  }

  getVariables() {
    return this.variables;
  }

  getEmail() {
    return this.email;
  }

  toJSON() {
    return {
      name: this.name,
      surname: this.surname,
      email: this.email
    };
  }

  setApiKey(key) {
    this.apiKey = key;
    this.binance = this.getBinance();
  }

  getApiKey() {
    return this.apiKey;
  }

  setApiSecret(key) {
    this.apiSecret = key;
    this.binance = this.getBinance();
  }

  getApiSecret() {
    return this.apiSecret;
  }

  getBinance() {
    return new BinanceRest.BinanceRest({
      key: this.getApiKey(),
      secret: this.getApiSecret(),
      baseUrl: process.env.TESTBINANCE_BASE_URL
    });
  }

  async getSymbolHistory(symbol){
    return await this.binance.allOrders(symbol);
  }
}

module.exports = User;
