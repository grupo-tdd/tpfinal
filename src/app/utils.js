const HTTPError = require('./error/HTTPError');
const dotenv = require('dotenv').config();
const crypto = require('crypto');
const CryptoJS = require("crypto-js");
const jwt = require('jsonwebtoken');
const cors = require('cors');
const messages = require("../../messages.json");
const moment = require('moment-timezone');

function invalidStringLength(s, minLen, maxLen) {
  return (s.length < minLen || s.length > maxLen);
}

function checkStringLength(s, minLen, maxLen, errMsg) {
  if ( s === undefined || invalidStringLength(s, minLen, maxLen) )
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, errMsg);
}

function invalidEmail(email) {
  return (email.length < 6 ||
    ( email.split("@").length - 1 ) !== 1 ||
    !email.includes(".") ||
    email.length > process.env.MAX_STR_LEN);
}

function checkInvalidEmail(email) {
  if ( invalidEmail(email) )
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, messages["INVALID_EMAIL"]);
}

function isAlphabeticString(s, errMsg) {
  if ( !s.match(/^[a-zA-Z]+$/i) )
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, errMsg);
}

function respond(res, status, res_body) {
  console.log(`Response status: ${status}\n`);
  // console.log(`Response status: ${status}\n` +
  //  `\tResponse body: ${JSON.stringify(res_body)}`);
  res.status(status).json(res_body);
}

function getHash(toHash) {
  return crypto.createHmac('md5', toHash).digest('hex');
}

function getNonAmericanDate(date) {
  let numbers = date.split("/");
  return numbers[1] + "/" + numbers[0] + "/" + numbers[2]
}

function checkDate(birthday) {
  if ( new Date( getNonAmericanDate(birthday) ).toString()
    === "Invalid Date" ) {
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, process.env.INVALID_DATE);
  }
}

function getTimestamp(){
    let date = new Date();
    return date.getTime();
}

function createSignature(query, secret) {
  return CryptoJS.HmacSHA256(query, secret).toString(CryptoJS.enc.Hex);
}

function createSellMarketQuery(symbol, amount, secret){
  let query = "";
  query += ( process.env.QUERY_SYMBOL + symbol );
  query += ( process.env.QUERY_SIDE + process.env.SELL_ACTION );
  query += ( process.env.QUERY_TYPE + process.env.MARKET_TYPE );
  query += ( process.env.QUERY_QUANTITY + amount.toString() );
  query += ( process.env.QUERY_NEW_CLIENT_ORDER_ID + "Sell_market_test_" + Date.now().toString() );
  query += ( process.env.QUERY_RESP_TYPE_AND_TIMESTAMP + getTimestamp() );
  query += ( process.env.QUERY_SIGNATURE + createSignature(query, secret) );
  return query;
}

function createBuyMarketQuery(symbol, amount, secret){
  let query = "";
  query += ( process.env.QUERY_SYMBOL + symbol );
  query += ( process.env.QUERY_SIDE + process.env.BUY_ACTION );
  query += ( process.env.QUERY_TYPE + process.env.MARKET_TYPE );
  query += ( process.env.QUERY_QUANTITY + amount.toString() );
  query += ( process.env.QUERY_NEW_CLIENT_ORDER_ID + "Buy_market_test_" + Date.now().toString() );
  query += ( process.env.QUERY_RESP_TYPE_AND_TIMESTAMP  + getTimestamp() );
  query += ( process.env.QUERY_SIGNATURE + createSignature(query, secret) );
  return query;
}

function createSymbol(symbol){
  const keys = symbol.split("/");
  return keys[0] + keys[1];
}

function flatArr(arr) {
  return arr.reduce((acc, val) => Array.isArray(val) ? acc.concat(flatArr(val)) : acc.concat(val), []);
}

function symbolIsValid(symbol) {
  let validSymbols = [
    process.env.BTC,
    process.env.ETH,
    process.env.BNB,
    process.env.USDT,
    process.env.BUSD
  ];
  return validSymbols.includes(symbol);
}

function symbolPairIsValid(symbol) {
  let validSymbols = [
    process.env.BTCBUSD,
    process.env.ETHBUSD,
    process.env.BNBBUSD,
    process.env.BNBUSDT,
    process.env.USDTBNB,
    process.env.BTCUSDT,
    process.env.ETHUSDT,
    process.env.USDTBTC,
    process.env.USDTETH
  ];
  return validSymbols.includes(symbol);
}

function jwtSign(email, uptime) {
  return jwt.sign({ email }, process.env.JWT_SECRET + uptime,
    { expiresIn: process.env.JWT_EXPIRE_TIME });
}

function jwtVerify(tokenValue, uptime) {
  return jwt.verify(tokenValue, process.env.JWT_SECRET + uptime, (err, decoded) => {
    if (err) {
      if (err.name === 'TokenExpiredError')
        throw new HTTPError(process.env.HTTP_401_UNAUTHORIZED, process.env.TOKEN_EXPIRED);
      else
        throw new HTTPError(process.env.HTTP_401_UNAUTHORIZED, process.env.TOKEN_INVALID);
    }
    // console.log("jwt decoded: ", decoded);
    return decoded;
  });
}

function encodeQuery(query) {
  if (query !== undefined) {
    return encodeURI(query);
  }

  return query;
}

function decodeQuery(query) {
  if (query !== undefined) {
    return decodeURI(query);
  }

  return query;
}

function getPrintableTimeStamp() {
  return moment().tz(process.env.TIMEZONE)
    .format(process.env.DATE_FORMAT).toString();
}

exports.checkStringLength = checkStringLength;
exports.isAlphabeticString = isAlphabeticString;
exports.invalidEmail = invalidEmail;
exports.checkInvalidEmail = checkInvalidEmail;
exports.respond = respond;
exports.getHash = getHash;
exports.checkDate = checkDate;
exports.getNonAmericanDate = getNonAmericanDate;
exports.createBuyMarketQuery = createBuyMarketQuery;
exports.createSellMarketQuery = createSellMarketQuery;
exports.createSymbol = createSymbol;
exports.flatArr = flatArr;
exports.symbolIsValid = symbolIsValid;
exports.symbolPairIsValid = symbolPairIsValid;
exports.jwtSign = jwtSign;
exports.jwtVerify = jwtVerify;
exports.decodeQuery = decodeQuery;
exports.encodeQuery = encodeQuery;
exports.getPrintableTimeStamp = getPrintableTimeStamp;
