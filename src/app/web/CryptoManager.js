const utils = require("../utils");
const Crypto = require("../model/Crypto");
const HTTPError = require("../error/HTTPError");
const messages = require("../../../messages.json");

class CryptoManager {
  constructor() {
    this.cryptos = {};
  }

  checkIfCryptoExists(cryptoCurr) {
    if (this.cryptos[cryptoCurr] === undefined) {
      throw new HTTPError(process.env.HTTP_404_NOT_FOUND,
        messages["CRYPTO_NOT_FOUND"]);
    }
  }

  getCrypto(crypto) {
    this.checkIfCryptoExists(crypto);
    return this.cryptos[crypto];
  }

  defineEvents(app) {
    app.get("/" + process.env.BTCBUSD, this.getBitcoinBusdValue.bind(this) );
  }

  async getBitcoinBusdValue(req, res) {
    console.log("GET Bitcoin value in BUSD.");

    await this.cryptos[process.env.BITCOIN].getBusdValue().then( result => {
      utils.respond( res,
        process.env.HTTP_200_OK, result );

    } );
  }

}

module.exports = CryptoManager;
