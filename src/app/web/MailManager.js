const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');
const mustache = require('mustache');
const emailInvitation = require('../mail/invitation-email-form');

const config = {
	auth: {
		api_key: process.env.SG_API_KEY
	}
};


class MailManager {

    constructor() {
		this.mailer = nodemailer.createTransport(sgTransport(config));
	}

    sendEmail(email) {

		this.mailer.sendMail(email, err => {
			if(err)
				console.log(err);
			else
				console.log(`Mensaje enviado a: ${email.to}`);
		});
	}

	sendInvitationEmail(hostEmail, guestEmail, ruleName, permission) {

		const emailBody = mustache.render(emailInvitation(hostEmail, guestEmail, ruleName, permission));

		const email = {
			from: 'tp.tdd.drive@gmail.com',
			to: guestEmail,
			subject: 'Invitación de acceso Crypto Rules',
			html: emailBody
		};

		this.sendEmail(email);
	}
}

module.exports = MailManager