const utils = require("../utils");
const HTTPError = require("../error/HTTPError");
const RuleParser = require("../RuleParser");
const Mutex = require("async-mutex").Mutex;
const util = require("util");
const messages = require("../../../messages.json");

class RuleManager {
  constructor() {
    this.rulesPerUser = {};
    this.ruleParser = new RuleParser();
    this.mutex = new Mutex();
    this.pendingRules = {};

  }

  getRule(name, email) {
    this.checkIfRuleDoestNotExist(name, email);
    return this.rulesPerUser[email]["rulesMap"][name];
  }

  async createRules(rules, user, requiredVariables, index = undefined) {
    if (!this.rulesPerUser[user.getEmail()])
      this.rulesPerUser[user.getEmail()] = { rules:[], rulesMap: {} };
    for (let i = 0; i < rules.length; i++ ) {
      this.checkRuleAttributeMissing(rules[i]["name"], rules[i]["condition"], rules[i]["action"]);
      this.checkIfRuleExist(rules[i]["name"], user.getEmail());
    }
    let newRules = this.ruleParser.parseRules(rules, user, requiredVariables);
    this.rules = Object.assign({}, this.rules, newRules);
    const release = await this.mutex.acquire();
    if (index === undefined){ 
      index = this.rulesPerUser[user.getEmail()]["rules"].length;
    }
    try {
      Object.keys(newRules).forEach((ruleName, ruleIndex) => {
        this.rulesPerUser[user.getEmail()]["rules"].splice(index + ruleIndex, 0, newRules[ruleName]);
        this.rulesPerUser[user.getEmail()]["rulesMap"][ruleName] = newRules[ruleName];
      });
    }
    finally {
      release();
    }
  }

  checkRuleAttributeMissing(ruleName, condition, action) {
    if ((ruleName === undefined) || (condition === undefined) || (action === undefined))
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        messages["RULE_BASIC_ATTRIBUTE_MISSING"]);
  }

  checkIfRuleExist(ruleName, email) {
    if ( this.rulesPerUser[email] &&  this.rulesPerUser[email]["rulesMap"][ruleName]  ){
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        messages["RULE_ALREADY_EXIST"]);
    }
  }

  checkIfRuleDoestNotExist(ruleName, email) {
    if ( ! this.rulesPerUser[email]["rulesMap"][ruleName] )
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        messages["RULE_NOT_EXIST"]);
  }

  async executeRules() {
    if (this.mutex.isLocked())
      return;
    
    const release = await this.mutex.acquire();
    try {
      const arr = Object.keys(this.rulesPerUser);
      await Promise.all(arr.map( async function (userEmail) {
        for (let index in this.rulesPerUser[userEmail]["rules"]) {
          await this.rulesPerUser[userEmail]["rules"][index].execute();
        }
      }.bind(this)));
    } finally {
      release();
    }
  }

  		
  async createPendingRule(srcEmail, dstUser, ruleName, permissionType){
    if (!this.pendingRules[dstUser.getEmail()])
        this.pendingRules[dstUser.getEmail()] = { rulesMap: {} };
    
    const release = await this.mutex.acquire();
    try {
      const ruleToShare = this.getRule(ruleName, srcEmail);
      const copyRule = this.copyRule(ruleToShare, dstUser, permissionType);
      this.pendingRules[dstUser.getEmail()]["rulesMap"][ruleName] = copyRule;
    }
    finally {
      release();
    }
  }

  copyRule(rule, user, permissionType){
     const copy = Object.assign(Object.create(Object.getPrototypeOf(rule)), rule);
     copy.changeUser(user);
     if ( permissionType == process.env.RO_PERMISSION ){
       copy.setReadOnlyPermission();
     }
     else{
       copy.setEditionPermission();
     }
     return copy;
  }

  async shareRule(ruleName, useremail){
    const release = await this.mutex.acquire();
    try {
      if (!this.rulesPerUser[useremail])
        this.rulesPerUser[useremail] = { rules:[], rulesMap: {} };
      const newRule = this.pendingRules[useremail]["rulesMap"][ruleName];
      this.rulesPerUser[useremail]["rulesMap"][ruleName] = newRule;
      this.rulesPerUser[useremail]["rules"].push(newRule);
      delete this.pendingRules[useremail]["rulesMap"][ruleName];
    }
    finally {
      release();
    } 
  }

  checkIfPendingRuleDoestNotExist(ruleName, email) {
    if ( ! this.pendingRules[email] || ! this.pendingRules[email]["rulesMap"][ruleName] )
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        messages["PENDING_RULE_NOT_EXIST"]);
  }


  async getRulesInfo(email) {
    let rulesInfo = { rules: [] }
    if (!this.rulesPerUser[email])
      return rulesInfo;
    const release = await this.mutex.acquire();
    try {
      Object.keys(this.rulesPerUser[email]["rulesMap"]).forEach(ruleName => {
        let ruleInfo = {
          name: ruleName,
          executed: this.rulesPerUser[email]["rulesMap"][ruleName].timesExecuted(),
          editor: this.rulesPerUser[email]["rulesMap"][ruleName].getEditionPermission()
        };
        rulesInfo["rules"].push(ruleInfo);
      });
    }
    finally {
      release();
    }
    return rulesInfo;
  }


async deleteRule(ruleName, email){
    this.checkIfRuleDoestNotExist(ruleName, email);
    const release = await this.mutex.acquire();
    let prevRule = {};
    try {
      delete this.rulesPerUser[email]["rulesMap"][ruleName];
      prevRule = this.deleteRuleFromArr(ruleName,email);
    }
    finally {
      release();
    }
    return prevRule;

  }

  deleteRuleFromArr(ruleName,email){
    let ruleIndex = 0;
    for( let i = 0; i < this.rulesPerUser[email]["rules"].length ;i++ ){
      if ( this.rulesPerUser[email]["rules"][i].getName() == ruleName ) {
        ruleIndex = i;
        break;
      }
    }
    let prevRule = {
      rule: this.rulesPerUser[email]["rules"][ruleIndex],
      index: ruleIndex
    };
    //console.log("Estoy eliminando la regla:", prevRule);
    this.rulesPerUser[email]["rules"].splice(ruleIndex, 1);
    //console.log("Devuelvo la regla eliminada:", prevRule);
    return prevRule;
  }

async getRuleFile(ruleName,email){
    const rule = this.getRule(ruleName,email);
    let file;
    const release = await this.mutex.acquire();
    try {
      file = rule.getJsonFile();
    }
    finally {
      release();
    }
    return file;
}

  async updateRule(name ,rules, user, requiredVariables) {
    let prevRule = await this.deleteRule(name, user.getEmail());
    try {
      await this.createRules(rules, user, requiredVariables, prevRule.index);
    } catch(err) {
      this.rulesPerUser[user.getEmail()]["rules"].splice(prevRule.index, 0, prevRule.rule);
      this.rulesPerUser[user.getEmail()]["rulesMap"][prevRule.rule.getName()] = prevRule.rule;
      throw err;
    }
  }

}

module.exports = RuleManager;
