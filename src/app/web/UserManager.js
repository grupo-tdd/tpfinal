const EventCompatible = require("../EventCompatible");
const HTTPError = require("../error/HTTPError");
const User = require("../model/User");
const utils = require("../utils");
const messages = require("../../../messages.json");
const moment = require('moment-timezone');

class UserManager extends EventCompatible {
  constructor() {
    super();
    this.uptime = moment().tz(process.env.TIMEZONE).valueOf().toString();
    this.users = {};
  }

  defineEvents(app) {
    app.post(process.env.SIGN_UP_URI, this.reqCreateUser.bind(this));
    app.post(process.env.LOG_IN_URI, this.getUserHash.bind(this));
    app.get(process.env.USER_URI + process.env.INFO_URI, this.getUserDefinition.bind(this));
    app.patch(process.env.USER_URI + process.env.INFO_URI, this.updateUser.bind(this));
    app.get(process.env.USER_URI + process.env.KEYS_URI, this.getKeys.bind(this));
    app.post(process.env.USER_URI + process.env.KEYS_URI, this.updateKeys.bind(this));
    app.get(process.env.USER_URI + process.env.VARIABLES_URI, this.getVariables.bind(this));
    app.put(process.env.USER_URI + process.env.VARIABLES_URI, this.updateVariable.bind(this));
    app.delete(process.env.USER_URI + process.env.VARIABLES_URI, this.deleteVariable.bind(this));
  }

  reqCreateUser(req, res) {
    console.log(`POST ${process.env.SIGN_UP_URI}`);
    // console.log(`\tRequest body: ${JSON.stringify(req.body)}`);

    try {
      this.createAUser(
        req.body.email,
        req.body.password,
        req.body.name,
        req.body.surname );
      utils.respond(res, process.env.HTTP_201_CREATED, this.users[req.body.email].toJSON());
    } catch (err) {
      this.handleEventError(res, err);
    }
  }

  getUserHash(req, res) {
    console.log(`POST ${process.env.LOG_IN_URI}`);
    try {
      this.checkUserIsValid(req.body.email, req.body.password);
      utils.respond(res, process.env.HTTP_200_OK, { userId: this.users[req.body.email].getUserId(this.uptime) });
    } catch(err) {
      this.handleEventError(res, err);
    }
  }

  getUserDefinition(req, res) {
    console.log(`GET ${process.env.USER_URI}${process.env.INFO_URI}`);
    try {
      let userId = req.headers["x-auth-token"];
      let email = this.getEmail(userId);
      let user = this.users[email];
      this.checkIfUserDoesNotExist(email);
      utils.respond(res, process.env.HTTP_200_OK, user.toJSON());
    } catch (err) {
      this.handleEventError(res, err);
    }
  }

  async updateUser(req, res) {
    console.log(`PATCH ${process.env.USER_URI}${process.env.INFO_URI}`);
    try {
      let userId = req.headers["x-auth-token"];
      let email = this.getEmail(userId);
      this.users[email].updateUser(req.body);
      this.checkIfUserDoesNotExist(email);
      // await this.users[email].persistUpdate();
      utils.respond(res, process.env.HTTP_200_OK, this.users[email].toJSON());
    } catch (err) {
      this.handleEventError(res, err);
    }
  }

  checkUserIsValid(email, password) {
    // console.log("received pass:", password);
    // console.log("hashed:", utils.getHash(password));

    if (!this.users[email] || !this.users[email].checkUserIsValid(email, password))
      throw new HTTPError(process.env.HTTP_401_UNAUTHORIZED, messages["INVALID_LOGIN_INFO"]);

  }

  
  createAUser(email, password, name, surname) {
    this.checkIfUserExists(email);
    this.users[email] = new User(email, password, name, surname);
    // console.log(this.users);
  }

  async getUserAllOrders(email, symbol){
    this.checkIfUserDoesNotExist(email);
    return await this.users[email].getSymbolHistory(symbol);
  }

  getUser(email){
    this.checkIfUserDoesNotExist(email);
    return this.users[email];
  }

  checkIfUserExists(email) {
    if ( this.users[email] )
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        messages["USER_ALREADY_EXIST"]);
  }

  checkIfUserDoesNotExist(email) {
    if ( ! this.users[email] )
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
        messages["USERID_NOT_FOUND"]);
  }
  
  async getAccountInfo(userId, res, symbol) {
    //console.log("user id:", userId);
    let email = this.getEmail(userId);
    //console.log("user mail:", email);
    this.checkIfUserDoesNotExist(email);
    utils.respond( res, process.env.HTTP_200_OK,
      await this.users[email].getWalletInfo(symbol) );
  }

  getEmail(userId) {
    let { email } = utils.jwtVerify(userId, this.uptime);
    this.checkIfUserDoesNotExist(email);
    return email;
  }

  async getKeys(req, res) {
    await this.handleEvent(req, res, `GET ${process.env.USER_URI + process.env.KEYS_URI}`,
      async (req, res) => {
        let userId = req.headers["x-auth-token"];
        const email = this.getEmail(userId);
        this.checkIfUserDoesNotExist(email);
        const user = this.users[email];
        utils.respond(res, process.env.HTTP_200_OK, {
          api_key: user.getApiKey(),
          api_secret: user.getApiSecret()
        });
      });
  }

  async updateKeys(req, res) {
    await this.handleEvent(req, res, `POST ${process.env.USER_URI + process.env.KEYS_URI}`,
      async (req, res) => {
        let userId = req.headers["x-auth-token"];
        const email = this.getEmail(userId);
        this.checkIfUserDoesNotExist(email);
        const prevApiKey = this.users[email].getApiKey();
        const prevApiSecret = this.users[email].getApiSecret();
        this.users[email].setApiKey(req.body.api_key);
        this.users[email].setApiSecret(req.body.api_secret);
        try {
          await this.users[email].getWalletInfo();
        } catch (e) {
          this.users[email].setApiKey(prevApiKey);
          this.users[email].setApiSecret(prevApiSecret);
          throw new HTTPError(process.env.HTTP_400_BAD_REQUEST,
            messages["INVALID_API_KEY_SECRET"]);
        }
        utils.respond(res, process.env.HTTP_200_OK, req.body);
      });
  }

  async updateVariable(req, res) {
    await this.handleEvent(req, res, `PUT ${process.env.USER_URI + process.env.VARIABLES_URI}`,
      async (req, res) => {
        let userId = req.headers["x-auth-token"];
        const email = this.getEmail(userId);
        this.checkIfUserDoesNotExist(email);
        this.users[email].setVariable(req.body.name, req.body.value);
        utils.respond(res, process.env.HTTP_200_OK, { name: req.body.name, value: req.body.value });
      });
  }

  async getVariables(req, res) {
    await this.handleEvent(req, res, `GET ${process.env.USER_URI + process.env.VARIABLES_URI}`,
      async (req, res) => {
        let userId = req.headers["x-auth-token"];
        const email = this.getEmail(userId);
        this.checkIfUserDoesNotExist(email);
        utils.respond(res, process.env.HTTP_200_OK, this.users[email].getVariables());
      });
  }

  async deleteVariable(req, res) {
    await this.handleEvent(req, res, `DELETE ${process.env.USER_URI + process.env.VARIABLES_URI}`,
      async (req, res) => {
        let userId = req.headers["x-auth-token"];
        const email = this.getEmail(userId);
        this.checkIfUserDoesNotExist(email);
        const prevValue = this.users[email].getVariable(req.body.name);
        this.users[email].setVariable(req.body.name, req.body.value);
        utils.respond(res, process.env.HTTP_200_OK, { name: req.body.name, value: prevValue });
      });
    }
}

module.exports = UserManager;
