const assert = require('assert');
const BuyMarketAction = require('../src/app/model/BuyMarketAction');
const CryptoTransactionAction = require('../src/app/model/CryptoTransactionAction');
const SellMarketAction = require('../src/app/model/SellMarketAction');
const SetVariableAction = require('../src/app/model/SetVariableAction');
const User = require('../src/app/model/User');
const ConditionConstant = require('../src/app/model/ConditionConstant');
const utils = require('../src/app/utils');
const fetchMock = require('fetch-mock');



describe('Test Actions Classes', function() {

    it("Test Crypto Transaction Action send POST request", async function(){
        const anAction = new CryptoTransactionAction("BNBUSDT", undefined);
        const endpoint = process.env.TESTBINANCE_ORDER_URL + utils.createBuyMarketQuery("BNBUSDT",0.1, process.env.BINANCE_API_SECRET);
        
        fetchMock.mock(endpoint,{symbol:"BNBUSDT"});

        const response = await anAction.sendPostRequest(endpoint);

        assert.strictEqual(response["symbol"],"BNBUSDT");

        fetchMock.restore();

    });


    it("Test Buy Market Action execute", async function(){
        const endpoint = process.env.TESTBINANCE_BASE_URL;
        const aCondition = new ConditionConstant(0.1);
        const anAction = new BuyMarketAction("BNBUSDT",aCondition);
        const aUser = new User("example@email.com","password","Name","Surname");

        fetchMock.mock("begin:"+endpoint,{symbol:"BNBUSDT"});

        const res = await anAction.executeAction(aUser);

        assert.strictEqual(res["symbol"], "BNBUSDT");

        fetchMock.restore();
    });


    it("Test Sell Market Action execute", async function(){
        const endpoint = process.env.TESTBINANCE_BASE_URL;
        const aCondition = new ConditionConstant(0.1);
        const anAction = new SellMarketAction("BNBUSDT",aCondition);
        const aUser = new User("example@email.com","password","Name","Surname");

        fetchMock.mock("begin:"+endpoint,{symbol:"BNBUSDT"});

        const res = await anAction.executeAction(aUser);

        assert.strictEqual(res["symbol"], "BNBUSDT");

        fetchMock.restore();
    });
    

    it("Test Set Variable Action execute, create variable", async function(){
        const aCondition = new ConditionConstant(10);
        const anAction = new SetVariableAction("aVariable",aCondition);
        const aUser = new User("example@email.com","password","Name","Surname");
        await anAction.executeAction(aUser);
        assert.strictEqual(aUser.getVariable("aVariable"),10);
    });


    it("Test Set Variable Action execute, set variable new value", async function(){
        const aCondition = new ConditionConstant(10);
        const anAction = new SetVariableAction("aVariable",aCondition);
        const aUser = new User("example@email.com","password","Name","Surname");
        aUser.setVariable("aVariable",5);
        assert.strictEqual(aUser.getVariable("aVariable"),5);
        await anAction.executeAction(aUser);
        assert.strictEqual(aUser.getVariable("aVariable"),10);
    });
  
  } );