const assert = require('assert');
const ConditionConstant = require('../src/app/model/ConditionConstant');
const ConditionFunction = require('../src/app/model/ConditionFunction');
const ConditionVariable = require('../src/app/model/ConditionVariable');
const functions = require("../src/app/functions");
const User = require('../src/app/model/User');
const ConditionWallet = require('../src/app/model/ConditionWallet');
const nock = require('nock');
const rewire = require("rewire");
const mockedDB = require("../src/app/db/MockedDB");
var ConditionData = rewire("../src/app/model/ConditionData");
ConditionData.__set__("CryptoBank", mockedDB);

describe('Test Condition Classes', function() {
    
    it('Test Condition constant check return value of the constant', async function(){
        const aConditionConstant = new ConditionConstant(true);
        const anUser = new User("example@gmail.com","password","Name","Surname");
        assert.strictEqual(aConditionConstant.check(anUser),true);
    });

    it('Test Condition variable check return value of the variable', async function(){
        const aConditionVariable = new ConditionVariable("aVariableName");
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("aVariableName", true);
        assert.strictEqual(await aConditionVariable.check(anUser),true);
    });

    it('Test Condition Function check for EQUAL function success', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '==', functions['=='], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 3);
        anUser.setVariable("variableNumber2", 3);
        anUser.setVariable("variableNumber3", 3);
        assert.strictEqual(await aConditionFunction.check(anUser),true);
    });

    it('Test Condition Function check for EQUAL function fail with distinct values', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '==', functions['=='], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 3);
        anUser.setVariable("variableNumber3", 3);
        assert.strictEqual(await aConditionFunction.check(anUser),false);
    });
    

    it('Test Condition Function check for DISTINCT function success', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionConstant = new ConditionConstant(8);
        const args = [ aConditionVariable1, aConditionVariable2, aConditionConstant ];
        const aConditionFunction = new ConditionFunction( 'DISTINCT', functions['DISTINCT'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 0);
        anUser.setVariable("variableNumber2", 1);
        assert.strictEqual(await aConditionFunction.check(anUser), true);
    });

    it('Test Condition Function check for DISTINCT function fails with equals values', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionConstant = new ConditionConstant(8);
        const args = [ aConditionVariable1, aConditionVariable2, aConditionConstant ];
        const aConditionFunction = new ConditionFunction( 'DISTINCT', functions['DISTINCT'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 0);
        anUser.setVariable("variableNumber2", 0);
        assert.strictEqual(await aConditionFunction.check(anUser), false);
    });

    it('Test Condition Function check for LESS THAN function success', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2= new ConditionVariable("variableNumber2");
        const aConditionConstant = new ConditionConstant(8);
        const args = [ aConditionVariable1, aConditionVariable2, aConditionConstant];
        const aConditionFunction = new ConditionFunction( '<', functions['<'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 0);
        anUser.setVariable("variableNumber2", 1);
        assert.strictEqual(await aConditionFunction.check(anUser),true);
    });

    it('Test Condition Function check for LESS THAN function fails with equals values', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2= new ConditionVariable("variableNumber2");
        const aConditionConstant = new ConditionConstant(8);
        const args = [ aConditionVariable1, aConditionVariable2, aConditionConstant];
        const aConditionFunction = new ConditionFunction( '<', functions['<'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 1);
        assert.strictEqual(await aConditionFunction.check(anUser),false);
    });

    it('Test Condition Function check for LESS AND EQUAL THAN function success', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '<=', functions['<='], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 0);
        anUser.setVariable("variableNumber2", 0);
        anUser.setVariable("variableNumber3", 1);
        assert.strictEqual(await aConditionFunction.check(anUser),true);
    });

    it('Test Condition Function check for LESS AND EQUAL THAN function fail with greater than value', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '<=', functions['<='], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 0);
        anUser.setVariable("variableNumber2", 2);
        anUser.setVariable("variableNumber3", 1);
        assert.strictEqual(await aConditionFunction.check(anUser),false);
    });


    it('Test Condition Function check for GREATER THAN function success', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '>', functions['>'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 5);
        anUser.setVariable("variableNumber2", 4);
        anUser.setVariable("variableNumber3", 2);
        assert.strictEqual(await aConditionFunction.check(anUser),true);
    });


    it('Test Condition Function check for GREATER THAN function fails with equals values', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '>', functions['>'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 5);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", 2);
        assert.strictEqual(await aConditionFunction.check(anUser),false);
    });

    it('Test Condition Function check for GREATER AND EQUAL THAN function success', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '>=', functions['>='], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 4);
        anUser.setVariable("variableNumber2", 2);
        anUser.setVariable("variableNumber3", 2);
        assert.strictEqual(await aConditionFunction.check(anUser),true);
    });

    it('Test Condition Function check for GREATER AND EQUAL THAN function fails with less than value', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '>=', functions['>='], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 2);
        anUser.setVariable("variableNumber3", 2);
        assert.strictEqual(await aConditionFunction.check(anUser),false);
    });

    it('Test Condition Function check for NEGATE function success', async function(){
        const aConditionVariable = new ConditionVariable("aVariable");
        const args = [ aConditionVariable ];
        const aConditionFunction = new ConditionFunction( 'NEGATE', functions['NEGATE'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("aVariable", ~7);
        assert.strictEqual(await aConditionFunction.check(anUser),7);
    });

    it('Test Condition Function check for NEGATE function fails', async function(){
        const aConditionVariable = new ConditionVariable("aVariable");
        const args = [ aConditionVariable ];
        const aConditionFunction = new ConditionFunction( 'NEGATE', functions['NEGATE'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("aVariable", ~7);
        assert.notStrictEqual(await aConditionFunction.check(anUser),6);
    });

    it('Test Condition Function check for NEGATE function throws error because not numeric operand', async function(){
        const aConditionVariable = new ConditionVariable("aVariable");
        const args = [ aConditionVariable ];
        const aConditionFunction = new ConditionFunction( 'NEGATE', functions['NEGATE'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        anUser.setVariable("aVariable", true);

        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'NEGATE' solo acepta valores de tipo number");
        }
        
    });

    it('Test Condition Function check for SUB function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumberCero");
        const aConditionVariable2 = new ConditionVariable("variableNumberOne");
        const args = [ aConditionVariable1, aConditionVariable2 ];
        const aConditionFunction = new ConditionFunction( '-', functions['-'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumberCero", 0);
        anUser.setVariable("variableNumberOne", 1);
        assert.strictEqual(await aConditionFunction.check(anUser), -1);
    });


    it('Test Condition Function check for SUB function throws error because not numeric operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumberCero");
        const aConditionVariable2 = new ConditionVariable("variableNumberOne");
        const args = [ aConditionVariable1, aConditionVariable2 ];
        const aConditionFunction = new ConditionFunction( '-', functions['-'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumberCero", 0);
        anUser.setVariable("variableNumberOne", "aString");

        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación '-' solo acepta valores de tipo number");
        }
    });

    it('Test Condition Function check for SUB function throws error invalid cant of args', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumberCero");
        const aConditionVariable2 = new ConditionVariable("variableNumberOne");
        const aConditionVariable3 = new ConditionVariable("variableNumberTwo");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3];
        const aConditionFunction = new ConditionFunction( '-', functions['-'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumberCero", 0);
        anUser.setVariable("variableNumberOne", 1);
        anUser.setVariable("variableNumberTwo", 3);

        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación '-' solo acepta 2 argumento/s");
        }
    });

    it('Test Condition Function check for DIV function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const args = [ aConditionVariable1, aConditionVariable2 ];
        const aConditionFunction = new ConditionFunction( '/', functions['/'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 4);
        assert.strictEqual(await aConditionFunction.check(anUser), 0.5);
    });

    it('Test Condition Function check for DIV function throws error because not numeric operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const args = [ aConditionVariable1, aConditionVariable2 ];
        const aConditionFunction = new ConditionFunction( '/', functions['/'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", "aString");
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación '/' solo acepta valores de tipo number");
        }
    });

    it('Test Condition Function check for DIV function throws error divided by zero', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const args = [ aConditionVariable1, aConditionVariable2 ];
        const aConditionFunction = new ConditionFunction( '/', functions['/'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 0);
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"No se puede dividir por 0");
        }
    });

    it('Test Condition Function check for DIV function throws error invalid cant of args', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const args = [ aConditionVariable1 ];
        const aConditionFunction = new ConditionFunction( '/', functions['/'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación '/' solo acepta 2 argumento/s");
        }
    });


    it('Test Condition Function check for ADD function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '+', functions['+'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 2);
        anUser.setVariable("variableNumber3", 3);
        assert.strictEqual(await aConditionFunction.check(anUser), 6);
    });


    it('Test Condition Function check for ADD function throws error because not numeric operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '+', functions['+'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 2);
        anUser.setVariable("variableNumber3", false);
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación '+' solo acepta valores de tipo number");
        }
    });


    it('Test Condition Function check for MULT function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '*', functions['*'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", 3);
        assert.strictEqual(await aConditionFunction.check(anUser), 30);
    });

    it('Test Condition Function check for MULT function throws error because not numeric operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( '*', functions['*'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", false);
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación '*' solo acepta valores de tipo number");
        }
    });

    it('Test Condition Function check for MIN function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'MIN', functions['MIN'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", 3);
        assert.strictEqual(await aConditionFunction.check(anUser), 2);
    });

    it('Test Condition Function check for MIN function throws error because not numeric operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'MIN', functions['MIN'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", true);
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'MIN' solo acepta valores de tipo number");
        }
    });

    it('Test Condition Function check for MAX function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'MAX', functions['MAX'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", 3);
        assert.strictEqual(await aConditionFunction.check(anUser), 5);
    });

    it('Test Condition Function check for MAX function throws error because not numeric operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'MAX', functions['MAX'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 2);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", "aString");
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'MAX' solo acepta valores de tipo number");
        }
    });


    it('Test Condition Function check for AVERAGE function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'AVERAGE', functions['AVERAGE'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", 3);
        assert.strictEqual(await aConditionFunction.check(anUser), 3);
    });

    it('Test Condition Function check for AVERAGE function throws error because not numeric operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'AVERAGE', functions['AVERAGE'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", true);
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'AVERAGE' solo acepta valores de tipo number");
        }
    });


    it('Test Condition Function check for STDDEV function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'STDDEV', functions['STDDEV'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", 3);
        assert.strictEqual(await aConditionFunction.check(anUser), Math.sqrt(8/3));
    });

    it('Test Condition Function check for STDDEV function throws error because not numeric operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'STDDEV', functions['STDDEV'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 5);
        anUser.setVariable("variableNumber3", "aString");

        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'STDDEV' solo acepta valores de tipo number");
        }
        
    });


    it('Test Condition Function check for STDDEV function throws error because array empty', async function(){
        const args = [];
        const aConditionFunction = new ConditionFunction( 'STDDEV', functions['STDDEV'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"STDDEV de un conjunto vacío");
        }
        
    });


    it('Test Condition Function check for NOT function success', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const argsTrue = [ aConditionVariable1 ];
        const argsFalse = [ aConditionVariable2 ];

        const aConditionFunction = new ConditionFunction( 'NOT', functions['NOT'], argsTrue );
        const anotherConditionFunction = new ConditionFunction( 'NOT', functions['NOT'], argsFalse );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", true);
        anUser.setVariable("variableNumber2", false);

        assert.strictEqual(await aConditionFunction.check(anUser), false);
        assert.strictEqual(await anotherConditionFunction.check(anUser), true);
        assert.notStrictEqual(await aConditionFunction.check(anUser), true);
        assert.notStrictEqual(await anotherConditionFunction.check(anUser), false);
    });

    it('Test Condition Function check for NOT function throws error invalid number of args', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const args = [ aConditionVariable1, aConditionVariable2 ];

        const aConditionFunction = new ConditionFunction( 'NOT', functions['NOT'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", true);
        anUser.setVariable("variableNumber2", false);
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'NOT' solo acepta 1 argumento/s");
        }
        
    });

    it('Test Condition Function check for NOT function throws error invalid because not boolean operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const args = [ aConditionVariable1 ];

        const aConditionFunction = new ConditionFunction( 'NOT', functions['NOT'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", "aString");
        
        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'NOT' solo acepta valores de tipo boolean");
        }
        
    });


    it('Test Condition Function check for AND function success with all true values', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'AND', functions['AND'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", true);
        anUser.setVariable("variableNumber2", true);
        anUser.setVariable("variableNumber3", true);
        assert.strictEqual(await aConditionFunction.check(anUser), true);
    });


    it('Test Condition Function check for AND function throws error invalid because not boolean operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'AND', functions['AND'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", true);
        anUser.setVariable("variableNumber2", false);
        anUser.setVariable("variableNumber3", "aString");

        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'AND' solo acepta valores de tipo boolean");
        }
    });


    it('Test Condition Function check for AND function fails if at least one false values', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'AND', functions['AND'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", false);
        anUser.setVariable("variableNumber2", true);
        anUser.setVariable("variableNumber3", true);
        assert.strictEqual(await aConditionFunction.check(anUser), false);
    });


    it('Test Condition Function check for AND function fails if all are false values', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'AND', functions['AND'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", false);
        anUser.setVariable("variableNumber2", false);
        anUser.setVariable("variableNumber3", false);
        assert.strictEqual(await aConditionFunction.check(anUser), false);
    });


    it('Test Condition Function check for OR function success with at least one true value', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'OR', functions['OR'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", false);
        anUser.setVariable("variableNumber2", true);
        anUser.setVariable("variableNumber3", false);
        assert.strictEqual(await aConditionFunction.check(anUser), true);
    });


    it('Test Condition Function check for OR function throws error invalid because not boolean operand', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'OR', functions['OR'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", false);
        anUser.setVariable("variableNumber2", false);
        anUser.setVariable("variableNumber3", 8);

        try{
            await aConditionFunction.check(anUser);
            assert.fail('La excepción esperada no fue lanzada');
        }catch ( error ){
            assert.strictEqual(error.message,"La operación 'OR' solo acepta valores de tipo boolean");
        }
    });


    it('Test Condition Function check for OR function fails if all are false values', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'OR', functions['OR'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", false);
        anUser.setVariable("variableNumber2", false);
        anUser.setVariable("variableNumber3", false);
        assert.strictEqual(await aConditionFunction.check(anUser), false);
    });


    it('Test Condition Function check for FIRST function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'FIRST', functions['FIRST'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 3);
        anUser.setVariable("variableNumber3", 5);
        assert.strictEqual(await aConditionFunction.check(anUser), 1);
    });


    it('Test Condition Function check for LAST function', async function(){
        const aConditionVariable1 = new ConditionVariable("variableNumber1");
        const aConditionVariable2 = new ConditionVariable("variableNumber2");
        const aConditionVariable3 = new ConditionVariable("variableNumber3");
        const args = [ aConditionVariable1, aConditionVariable2, aConditionVariable3 ];
        const aConditionFunction = new ConditionFunction( 'LAST', functions['LAST'], args );
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        anUser.setVariable("variableNumber1", 1);
        anUser.setVariable("variableNumber2", 3);
        anUser.setVariable("variableNumber3", 5);
        assert.strictEqual(await aConditionFunction.check(anUser), 5);
    });

    it('Test Condition Wallet check for valid symbol', async function(){

        var scope = nock(process.env.TESTBINANCE_BASE_URL,{
            reqheaders:{
                "x-mbx-apikey": process.env.BINANCE_API_KEY,
                "host": "testnet.binance.vision"
            }
        })
            .persist()
            .get("/api/v3/account")
            .query(true)
            .reply(200, {
                balances: [
                        {
                            "asset": "ETH",
                            "free": "105.000000",
                            "locked": "0.00000000"
                        }
                ]
            });

        const aConditionWallet = new ConditionWallet("ETH");
        let anUser = new User("example@gmail.com","password","Name","Surname");
        assert.strictEqual( await aConditionWallet.check(anUser),105.0);
        nock.cleanAll();
    });

    it('Test Condition Wallet check for invalid symbol return undefined', async function(){

        var scope = nock(process.env.TESTBINANCE_BASE_URL,{
            reqheaders:{
                "x-mbx-apikey": process.env.BINANCE_API_KEY,
                "host": "testnet.binance.vision"
            }
        })
            .persist()
            .get("/api/v3/account")
            .query(true)
            .reply(200, {
                balances: [
                        {
                            "asset": "BNB",
                            "free": "1111.00000000",
                            "locked": "0.00000000"
                        }
                ]
            });

        const aConditionWallet = new ConditionWallet("BTC");
        let anUser = new User("example@gmail.com","password","Name","Surname");
        assert.strictEqual( await aConditionWallet.check(anUser),undefined);
        nock.cleanAll();
    });


    it('Test Condition Data check returns an array of prices for the symbol if time period exist', async function(){

        const aConditionData = new ConditionData("BNBUSDT",86400,0,undefined);
        let anUser = new User("example@gmail.com","password","Name","Surname");
        const values = await aConditionData.check(anUser);
        assert.strictEqual(values[0],500);
        assert.strictEqual(values[1],480);
    });

});