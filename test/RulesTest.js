const User = require('../src/app/model/User');
const ConditionWallet = require('../src/app/model/ConditionWallet');
const ConditionVariable = require('../src/app/model/ConditionVariable');
const ConditionFunction = require('../src/app/model/ConditionFunction');
const ConditionConstant = require('../src/app/model/ConditionConstant');
const SellMarketAction = require('../src/app/model/SellMarketAction');
const functions = require("../src/app/functions");
const nock = require('nock');
const fetchMock = require('fetch-mock');
const Rule = require('../src/app/model/Rule');
const rewire = require("rewire");
const mockedDB = require("../src/app/db/MockedDB");
var ConditionData = rewire("../src/app/model/ConditionData");
ConditionData.__set__("CryptoBank", mockedDB);
const chai = require('chai');
const expect = chai.expect;
const spies = require('chai-spies');
const SetVariableAction = require('../src/app/model/SetVariableAction');
const { assert } = require('chai');
chai.use(spies);



describe('Test Rules Classes', function() {

    it ("Test rule example 1 execute", async function(){
      //User creation
      let anUser = new User("example@gmail.com","password","Name","Surname");
      //required variable creation (this value will make the condition of the rule return true => the actions will execute )
      anUser.setVariable("LIMIT_VALUE_BTC/USDT",600.0);

      //binance account query mocked
      var scope = nock(process.env.TESTBINANCE_BASE_URL,{
        reqheaders:{
            "x-mbx-apikey": process.env.BINANCE_API_KEY,
            "host": "testnet.binance.vision"
        }
    })
        .persist()
        .get("/api/v3/account")
        .query(true)
        .reply(200, {
            balances: [
                    {
                        "asset": "BTC",
                        "free": "10050.000000",
                        "locked": "0.00000000"
                    },
                    {
                      "asset": "ETH",
                      "free": "177.000000",
                      "locked": "0.00000000"
                  }
            ]
        });

        //Binance SELL MARKET action query mocked
        const endpoint = process.env.TESTBINANCE_BASE_URL;
        fetchMock.mock("begin:"+endpoint,{symbol:"BTCUSDT"});
      
        //Create Condition Wallet
        const aConditionWallet = new ConditionWallet("BTC");
        //Create Condition Variable
        const aConditionVariable = new ConditionVariable("LIMIT_VALUE_BTC/USDT");
        //Create Condition Data
        const aConditionData = new ConditionData("BTCUSDT",3600,0,aConditionVariable);
        
        //Create Condition Functions
        const aConditionFunctionLast = new ConditionFunction("LAST", functions["LAST"], [aConditionData]);
        const aConditionFunctionLesserThan = new ConditionFunction("<", functions["<"], [aConditionFunctionLast, aConditionVariable]);

        //Create SELL_MARKET action
        const aSellMarketAction = new SellMarketAction("BTCUSDT",aConditionWallet);

        //Create array of actions
        const arr = [aSellMarketAction];

        //Required Variables
        const requiredVariables = ["LIMIT_VALUE_BTC/USDT"];

        //Create Rule Example 1
        const aRule = new Rule("Escape",aConditionFunctionLesserThan,arr,anUser,{},requiredVariables);

        
        
        //with chai we test if 'executeActions' was execute, this means the conditions were true
        
        let spy = chai.spy.on(aRule, 'executeActions');

        //Execute rule
        await aRule.execute();

        expect(spy).to.have.been.called();

        fetchMock.restore();
        nock.cleanAll();

    });

    
    it ("Test rule example 1 won't execute if required variable doesn't exist", async function(){
        //User creation
        let anUser = new User("example@gmail.com","password","Name","Surname");
  
        //binance account query mocked
        var scope = nock(process.env.TESTBINANCE_BASE_URL,{
          reqheaders:{
              "x-mbx-apikey": process.env.BINANCE_API_KEY,
              "host": "testnet.binance.vision"
          }
      })
          .persist()
          .get("/api/v3/account")
          .query(true)
          .reply(200, {
              balances: [
                      {
                          "asset": "BTC",
                          "free": "10050.000000",
                          "locked": "0.00000000"
                      },
                      {
                        "asset": "ETH",
                        "free": "177.000000",
                        "locked": "0.00000000"
                    }
              ]
          });
  
          //Binance SELL MARKET action query mocked
          const endpoint = process.env.TESTBINANCE_BASE_URL;
          fetchMock.mock("begin:"+endpoint,{symbol:"BTCUSDT"});
        
          //Create Condition Wallet
          const aConditionWallet = new ConditionWallet("BTC");
          //Create Condition Variable
          const aConditionVariable = new ConditionVariable("LIMIT_VALUE_BTC/USDT");
          //Create Condition Data
          const aConditionData = new ConditionData("BTCUSDT",3600,0,aConditionVariable);
          
          //Create Condition Functions
          const aConditionFunctionLast = new ConditionFunction("LAST", functions["LAST"], [aConditionData]);
          const aConditionFunctionLesserThan = new ConditionFunction("<", functions["<"], [aConditionFunctionLast, aConditionVariable]);
  
          //Create SELL_MARKET action
          const aSellMarketAction = new SellMarketAction("BTCUSDT",aConditionWallet);
  
          //Create array of actions
          const arr = [aSellMarketAction];

          //Required Variables
          const requiredVariables = ["LIMIT_VALUE_BTC/USDT"];
  
          //Create Rule Example 1
          const aRule = new Rule("Escape",aConditionFunctionLesserThan,arr,anUser,{},requiredVariables);
  
          
          //with chai we test if 'executeActions' was execute, this means the conditions were true
          
          let spy = chai.spy.on(aRule, 'executeActions');
  
          //Execute rule
          await aRule.execute();
  
          expect(spy).not.to.have.been.called();
  
          fetchMock.restore();
          nock.cleanAll();
          
      });

      it ("Test rule example 1 won't execute if conditions are false", async function(){
        //User creation
        let anUser = new User("example@gmail.com","password","Name","Surname");
        //required variable creation (this value will make the condition of the rule return false => the actions won't execute )
        anUser.setVariable("LIMIT_VALUE_BTC/USDT",300.0);
  
        //binance account query mocked
        var scope = nock(process.env.TESTBINANCE_BASE_URL,{
          reqheaders:{
              "x-mbx-apikey": process.env.BINANCE_API_KEY,
              "host": "testnet.binance.vision"
          }
        })
          .persist()
          .get("/api/v3/account")
          .query(true)
          .reply(200, {
              balances: [
                      {
                          "asset": "BTC",
                          "free": "10050.000000",
                          "locked": "0.00000000"
                      },
                      {
                        "asset": "ETH",
                        "free": "177.000000",
                        "locked": "0.00000000"
                    }
              ]
          });
  
          //Binance SELL MARKET action query mocked
          const endpoint = process.env.TESTBINANCE_BASE_URL;
          fetchMock.mock("begin:"+endpoint,{symbol:"BTCUSDT"});
        
          //Create Condition Wallet
          const aConditionWallet = new ConditionWallet("BTC");
          //Create Condition Variable
          const aConditionVariable = new ConditionVariable("LIMIT_VALUE_BTC/USDT");
          //Create Condition Data
          const aConditionData = new ConditionData("BTCUSDT",3600,0,aConditionVariable);
          
          //Create Condition Functions
          const aConditionFunctionLast = new ConditionFunction("LAST", functions["LAST"], [aConditionData]);
          const aConditionFunctionLesserThan = new ConditionFunction("<", functions["<"], [aConditionFunctionLast, aConditionVariable]);
  
          //Create SELL_MARKET action
          const aSellMarketAction = new SellMarketAction("BTCUSDT",aConditionWallet);
  
          //Create array of actions
          const arr = [aSellMarketAction];

          //Required Variables
          const requiredVariables = ["LIMIT_VALUE_BTC/USDT"];
  
          //Create Rule Example 1
          const aRule = new Rule("Escape",aConditionFunctionLesserThan,arr,anUser,{},requiredVariables);
  
          
          
          //with chai we test if 'executeActions' was execute, this means the conditions were true
          
          let spy = chai.spy.on(aRule, 'executeActions');
  
          //Execute rule
          await aRule.execute();
  
          expect(spy).not.to.have.been.called();
  
          fetchMock.restore();
          nock.cleanAll();
  
      });

      it("Test example 2 execute", async function(){
        
        //User creation
        let anUser = new User("example@gmail.com","password","Name","Surname");
        //required variable creation (this value will make the condition of the rule return true => the actions will execute )
        anUser.setVariable("LAST_SELL_VALUE_BTC/USDT",600.0);

        
        //Binance SELL MARKET action query mocked
        const endpoint = process.env.TESTBINANCE_BASE_URL;
        fetchMock.mock("begin:"+endpoint,{symbol:"BTCUSDT"});

        //Create Condition Variable
        const aConditionVariable = new ConditionVariable("LAST_SELL_VALUE_BTC/USDT");

        //Create Condition Constant with value 1.15
        const aConditionConstant1 = new ConditionConstant(1.15);

        //Create Condition Constant with value 0.1
        const aConditionConstant2 = new ConditionConstant(0.1);

        //Create Condition Data

        const aConditionData = new ConditionData("BTCUSDT",3600,0,undefined);

        //Create Condition Function Last

        const aConditionFunctionLast = new ConditionFunction("LAST", functions["LAST"], [aConditionData]);

        //Create Condition Function Mult

        const aConditionFunctionMult = new ConditionFunction("*", functions["*"], [aConditionConstant1, aConditionVariable]);

        //Create Condition Function Greater Than

        const aConditionFunctionGreaterThan = new ConditionFunction(">", functions[">"], [aConditionFunctionMult, aConditionFunctionLast]);

        //Create Sell Market Action
        
        const aSellMarketAction = new SellMarketAction("BTCUSDT",aConditionConstant2);

        //Create Set Variable Action
        
        const aSetVariableAction = new SetVariableAction("LAST_SELL_VALUE_BTC/USDT",aConditionFunctionLast);

        //Create actions array

        const arr = [  aSellMarketAction, aSetVariableAction ];

        //Required Variables
        const requiredVariables = ["LAST_SELL_VALUE_BTC/USDT"];

        //Create Rule Example 1
        const aRule = new Rule("Vender si sube 15%",aConditionFunctionGreaterThan,arr,anUser,{},requiredVariables);

        //with chai we test if 'executeActions' was execute, this means the conditions were true
          
        let spy = chai.spy.on(aRule, 'executeActions');
        
        //Check variable value is 600
        assert.strictEqual(anUser.getVariable("LAST_SELL_VALUE_BTC/USDT"),600);

        //Execute rule
        await aRule.execute();
        expect(spy).to.have.been.called();

        //Check variable new value is 480
        assert.strictEqual(anUser.getVariable("LAST_SELL_VALUE_BTC/USDT"),480);
        
        fetchMock.restore();

      });

      
      it("Test example 2 won't execute if required variable doesn't exist", async function(){
        //User creation
        let anUser = new User("example@gmail.com","password","Name","Surname");
        
        //Binance SELL MARKET action query mocked
        const endpoint = process.env.TESTBINANCE_BASE_URL;
        fetchMock.mock("begin:"+endpoint,{symbol:"BTCUSDT"});

        //Create Condition Variable
        const aConditionVariable = new ConditionVariable("LAST_SELL_VALUE_BTC/USDT");

        //Create Condition Constant with value 1.15
        const aConditionConstant1 = new ConditionConstant(1.15);

        //Create Condition Constant with value 0.1
        const aConditionConstant2 = new ConditionConstant(0.1);

        //Create Condition Data

        const aConditionData = new ConditionData("BTCUSDT",3600,0,undefined);

        //Create Condition Function Last

        const aConditionFunctionLast = new ConditionFunction("LAST", functions["LAST"], [aConditionData]);

        //Create Condition Function Mult

        const aConditionFunctionMult = new ConditionFunction("*", functions["*"], [aConditionConstant1, aConditionVariable]);

        //Create Condition Function Greater Than

        const aConditionFunctionGreaterThan = new ConditionFunction(">", functions[">"], [aConditionFunctionMult, aConditionFunctionLast]);

        //Create Sell Market Action
        
        const aSellMarketAction = new SellMarketAction("BTCUSDT",aConditionConstant2);

        //Create Set Variable Action
        
        const aSetVariableAction = new SetVariableAction("LAST_SELL_VALUE_BTC/USDT",aConditionFunctionLast);

        anUser.setVariable("LAST_SELL_VALUE_BTC/USDT",12345);
        //Create actions array

        const arr = [  aSellMarketAction, aSetVariableAction ];

        //Required Variables
        const requiredVariables = ["LAST_SELL_VALUE_BTC/TDD"];

        //Create Rule Example 1
        const aRule = new Rule("Vender si sube 15%",aConditionFunctionGreaterThan,arr,anUser,{},requiredVariables);


        //with chai we test if 'executeActions' was execute, this means the conditions were true
          
        let spy = chai.spy.on(aRule, 'executeActions');
        
        //Execute rule
        await aRule.execute();
        expect(spy).not.to.have.been.called();
        
        fetchMock.restore();

      });
      

     it("Test example 2 execute won't execute if conditions are false", async function(){
        
        //User creation
        let anUser = new User("example@gmail.com","password","Name","Surname");
        //required variable creation (this value will make the condition of the rule return false => the actions won't execute )
        anUser.setVariable("LAST_SELL_VALUE_BTC/USDT",300.0);

        
        //Binance SELL MARKET action query mocked
        const endpoint = process.env.TESTBINANCE_BASE_URL;
        fetchMock.mock("begin:"+endpoint,{symbol:"BTCUSDT"});

        //Create Condition Variable
        const aConditionVariable = new ConditionVariable("LAST_SELL_VALUE_BTC/USDT");

        //Create Condition Constant with value 1.15
        const aConditionConstant1 = new ConditionConstant(1.15);

        //Create Condition Constant with value 0.1
        const aConditionConstant2 = new ConditionConstant(0.1);

        //Create Condition Data

        const aConditionData = new ConditionData("BTCUSDT",3600,0,undefined);

        //Create Condition Function Last

        const aConditionFunctionLast = new ConditionFunction("LAST", functions["LAST"], [aConditionData]);

        //Create Condition Function Mult

        const aConditionFunctionMult = new ConditionFunction("*", functions["*"], [aConditionConstant1, aConditionVariable]);

        //Create Condition Function Greater Than

        const aConditionFunctionGreaterThan = new ConditionFunction(">", functions[">"], [aConditionFunctionMult, aConditionFunctionLast]);

        //Create Sell Market Action
        
        const aSellMarketAction = new SellMarketAction("BTCUSDT",aConditionConstant2);

        //Create Set Variable Action
        
        const aSetVariableAction = new SetVariableAction("LAST_SELL_VALUE_BTC/USDT",aConditionFunctionLast);

        //Create actions array

        const arr = [  aSellMarketAction, aSetVariableAction ];

        //Required Variables
        const requiredVariables = ["LAST_SELL_VALUE_BTC/TDD"];

        //Create Rule Example 1
        const aRule = new Rule("Vender si sube 15%",aConditionFunctionGreaterThan,arr,anUser,{},requiredVariables);


        //with chai we test if 'executeActions' was execute, this means the conditions were true
          
        let spy = chai.spy.on(aRule, 'executeActions');
        
        //Check variable value is 300
        assert.strictEqual(anUser.getVariable("LAST_SELL_VALUE_BTC/USDT"),300);

        //Execute rule
        await aRule.execute();
        expect(spy).not.to.have.been.called();

        //Check variable new value is still 300
        assert.strictEqual(anUser.getVariable("LAST_SELL_VALUE_BTC/USDT"),300);
        
        fetchMock.restore();

      });


  } );