const assert = require('assert');
const Crypto = require('../src/app/model/Crypto');
const User = require('../src/app/model/User');
const HTTPError = require("../src/app/error/HTTPError");
const dotenv = require('dotenv').config();
const nock = require('nock');



describe('Test User Class', function() {
    it('Test User creation', function(){
        const aUser = new User("example@email.com","password","Name","Surname");
        assert.strictEqual(aUser.name, "Name");
        assert.strictEqual(aUser.surname, "Surname");
        assert.strictEqual(aUser.email, "example@email.com");
    });


    it('Test User set variable', function(){
        const aUser = new User("example@email.com","password","Name","Surname");
        aUser.setVariable("aVariable",1000);
        assert.strictEqual(aUser.getVariable("aVariable"),1000);
    });


    it('Test User check if variable exist when a variable does not exist', function(){
        const aUser = new User("example@email.com","password","Name","Surname");
        assert.strictEqual(aUser.checkIfVariableExists("aVariable"),undefined);
    });


    it('Test User check if variable exist when a variable exist', function(){
        const aUser = new User("example@email.com","password","Name","Surname");
        aUser.setVariable("aVariable",1000);
        assert.throws(
            function(){aUser.checkIfVariableExists("aVariable")},
            new HTTPError(process.env.HTTP_400_BAD_REQUEST,process.env.VARIABLE_ALREADY_EXISTS));
    });

    it('Test User get wallet info', async function(){

        var scope = nock(process.env.TESTBINANCE_BASE_URL,{
            reqheaders:{
                "x-mbx-apikey": process.env.BINANCE_API_KEY,
                "host": "testnet.binance.vision"
            }
        })
            .persist()
            .get("/api/v3/account")
            .query(true)
            .reply(200, {
                balances: [
                        {
                            "asset": "BNB",
                            "free": "1111.00000000",
                            "locked": "0.00000000"
                        },
                        {
                            "asset": "BTC",
                            "free": "1.17000000",
                            "locked": "0.00000000"
                        },
                        {
                            "asset": "BUSD",
                            "free": "4048.000000",
                            "locked": "0.00000000"
                        },
                        {
                            "asset": "ETH",
                            "free": "105.000000",
                            "locked": "0.00000000"
                        }
                ]
            });
        
        const aUser = new User("example@email.com","password","Name","Surname");
        assert.strictEqual(await aUser.getWalletInfo("BNB"),1111);
        assert.strictEqual(await aUser.getWalletInfo("BTC"),1.17);
        assert.strictEqual(await aUser.getWalletInfo("BUSD"),4048);
        assert.strictEqual(await aUser.getWalletInfo("ETH"),105);
        nock.cleanAll();
    });
    
  } );